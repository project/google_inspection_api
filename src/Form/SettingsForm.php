<?php

namespace Drupal\google_inspection_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\file\Entity\File;


/**
 * Class Settingsform
 */
class SettingsForm extends FormBase {

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal\file\FileUsage\FileUsageInterface definition.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The State Service.
   * @param \Drupal\file\FileUsage\FileUsageInterface $file_usage
   *   The file usage service.
   */
  public function __construct(StateInterface $state, FileUsageInterface $fileUsage) {
    $this->state = $state;
    $this->fileUsage = $fileUsage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('file.usage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_inspection_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['instructions'] = [
      '#type' => 'fieldset',
      '#title' => t('Instructions to obtain a OAuth JSON File'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['instructions']['steps'] = [
      '#type' => 'inline_template',
      '#title' => t('Instructions to obtain a OAuth JSON File'),
      '#disabled' => TRUE,
      '#format' => 'full_html',
      '#template' => '{% trans %}<ol>
<li>Open the <a href="https://console.developers.google.com/apis/credentials">Credentials page</a> in the API Console.</li>
<li>Click <strong>Create credentials &gt; OAuth client ID</strong>.</li>
<li>Choose application type "Web application"</li>
<li>Add "{{url}}" as an authorized redirect URI</li>
<li>Save it and dowload the client secret as a json file</li>
</ol>{% endtrans %}',
      '#context' => [
        'url' => Url::fromRoute('google_inspection_api.google_inspection_api_dashboard', [], ['absolute' => TRUE])
          ->toString(),
      ],
    ];

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['settings']['google_inspection_api_json_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('OAuth JSON File'),
      '#description' => 'This file contains your private key and other information. <br /> Follow <a href="https://github.com/googleapis/google-api-php-client/blob/main/docs/oauth-web.md#create-authorization-credentials">these instructions</a> to set up and obtain your json file.',
      '#default_value' => $this->state->get('google_inspection_api_json_file'),
      '#upload_location' => 'private://google_inspection_api/',
      '#upload_validators' => [
        'file_validate_extensions' => ['json'],
      ],
      '#required' => TRUE,
    ];
    $form['settings']['google_inspection_api_inspecturl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('inspectUrl'),
      '#description' => 'Required. <br />Fully-qualified URL to inspect. Must be under the property specified in "siteUrl".',
      '#default_value' => $this->state->get('google_inspection_api_inspecturl'),
      '#required' => TRUE,
    ];
    $form['settings']['google_inspection_api_siteurl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('siteUrl'),
      '#description' => 'Required. <br />The URL of the property as defined in Search Console. Note that URL-prefix properties must include a trailing / mark. Examples: https://www.example.com/ for a URL-prefix property, or sc-domain:example.com for a Domain property.',
      '#default_value' => $this->state->get('google_inspection_api_siteurl'),
      '#required' => TRUE,
    ];
    $form['settings']['google_inspection_api_languagecode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('languageCode'),
      '#description' => 'Optional. <br />An IETF BCP-47 language code representing the requested language for translated issue messages, e.g. "en-US", "or "de-CH". Default value is "en-US".".',
      '#default_value' => empty($this->state->get('google_inspection_api_languagecode')) ? 'en-US' : $this->state->get('google_inspection_api_languagecode'),
      '#required' => FALSE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fid = $form_state->getValue('google_inspection_api_json_file');
    $inspectUrl = $form_state->getValue('google_inspection_api_inspecturl');
    $siteUrl = $form_state->getValue('google_inspection_api_siteurl');
    $languageCode = $form_state->getValue('google_inspection_api_languagecode');

    if (isset($fid[0])) {
      $file = File::load($fid[0]);
      if ($file !== NULL) {
        $file->status = FILE_STATUS_PERMANENT;
        $file->save();
        $uuid = $file->uuid();
        $this->fileUsage->add($file, 'google_inspection_api', 'google_inspection_api', $uuid);
      }
      else {
        $fid = NULL;
      }
    }
    else {
      $fid = NULL;
    }

    // Save the settings.
    $this->state->set('google_inspection_api_json_file', $fid);
    $this->state->set('google_inspection_api_inspecturl', $inspectUrl);
    $this->state->set('google_inspection_api_siteurl', $siteUrl);
    $this->state->set('google_inspection_api_languagecode', $languageCode);
  }

}
