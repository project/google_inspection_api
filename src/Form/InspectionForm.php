<?php

namespace Drupal\google_inspection_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\google_inspection_api\Services\GoogleInspectionApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Class InspectionForm
 */
class InspectionForm extends FormBase {

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The Google Inspection API Service.
   *
   * @var \Drupal\google_inspection_api\Services\GoogleInspectionApi
   */
  protected $googleInspectionApi;

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The State Service.
   *
   * @param \Drupal\google_inspection_api\Services\GoogleInspectionApi $googleInspectionApi
   *  The Google Inspection API Service.
   */
  public function __construct(StateInterface $state, GoogleInspectionApi $googleInspectionApi) {
    $this->state = $state;
    $this->googleInspectionApi = $googleInspectionApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('google_inspection_api.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_inspection_api_dashboard';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['dashboard'] = [
      '#type' => 'fieldset',
      '#title' => t('Dashboard'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['dashboard']['notice'] = [
      '#type' => 'inline_template',
      '#open' => TRUE,
      '#template' => '<p style="color: #970f00; font-weight: bolder">{% trans %}You currently have to click the Inspect button twice to get results{% endtrans %}</p>',
    ];
    $form['dashboard']['inspect'] = [
      '#type' => 'submit',
      '#value' => t('Inspect'),
    ];
    $form['result'] = [
      '#type' => 'fieldset',
      '#title' => t('Result'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $link_result_value = $form_state->getValue('linkResult');
    if ($link_result_value && $link_result_value != 'Inspection Result Link') {
      $form['result']['linkResult'] = [
        '#type' => 'button',
        '#value' => t('Inspection Result Link'),
        '#attributes' => [
          'type' => 'button',
          'onclick' => 'window.open("' . $link_result_value . '", "_blank")',
        ],
        "#executes_submit_callback" => FALSE,
      ];
    }

    $index_result_value = $form_state->getValue('indexResult');
    if ($index_result_value) {
      $form['result']['indexResult'] = [
        '#type' => 'textarea',
        '#title' => t('Index Result'),
        '#default_value' => $index_result_value ? $index_result_value : '',
        '#rows' => 20,
        '#disabled' => TRUE,
      ];
    }

    $mobile_result_value = $form_state->getValue('mobileResult');
    if ($mobile_result_value) {
      $form['result']['mobileResult'] = [
        '#type' => 'textarea',
        '#title' => t('Mobile Usability Result'),
        '#default_value' => $mobile_result_value ? $mobile_result_value : '',
        '#rows' => 20,
        '#disabled' => TRUE,
      ];
    }

    $amp_result_value = $form_state->getValue('ampResult');
    if ($amp_result_value) {
      $form['result']['ampResult'] = [
        '#type' => 'textarea',
        '#title' => t('Amp Result'),
        '#default_value' => $amp_result_value ? $amp_result_value : '',
        '#rows' => 20,
        '#disabled' => TRUE,
      ];
    }

    $rich_result_value = $form_state->getValue('richResult');
    if ($rich_result_value) {
      $form['result']['richResult'] = [
        '#type' => 'textarea',
        '#title' => t('Rich Result'),
        '#default_value' => $rich_result_value ? $rich_result_value : '',
        '#rows' => 20,
        '#disabled' => TRUE,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Inspect the url.
    $result = $this->googleInspectionApi->inspect();
    // Set the result values.
    if (!empty($result->getInspectionResult()->getIndexStatusResult())) {
      $form_state->setValue('indexResult', print_r(get_object_vars($result->getInspectionResult()
        ->getIndexStatusResult()->toSimpleObject()), TRUE));
    }
    if (!empty($result->getInspectionResult()->getMobileUsabilityResult())) {
      $form_state->setValue('mobileResult', print_r(get_object_vars($result->getInspectionResult()
        ->getMobileUsabilityResult()->toSimpleObject()), TRUE));
    }
    if (!empty($result->getInspectionResult()->getAmpResult())) {
      $form_state->setValue('ampResult', print_r(get_object_vars($result->getInspectionResult()
        ->getAmpResult()->toSimpleObject()), TRUE));
    }
    if (!empty($result->getInspectionResult()->getRichResultsResult())) {
      $form_state->setValue('richResult', print_r(get_object_vars($result->getInspectionResult()
        ->getRichResultsResult()->toSimpleObject()), TRUE));
    }
    if (!empty($result->getInspectionResult()->getInspectionResultLink())) {
      $form_state->setValue('linkResult', $result->getInspectionResult()
        ->getInspectionResultLink());
    }
    // Rebuild the form.
    $form_state->setRebuild();
  }

}
