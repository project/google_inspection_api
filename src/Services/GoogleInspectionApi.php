<?php

namespace Drupal\google_inspection_api\Services;

use Drupal\Core\State\StateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\file\Entity\File;
use Exception;

/*
 * Service class for calling the Google Inspection API.
 *
 * @ingroup google_inpection_api
 */

class GoogleInspectionApi {

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /*
   * The Google Client
   *
   * @var \Google_Client
   */
  protected $client;

  /**
   * Callback Controller constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The State Service.
   */
  public function __construct(StateInterface $state, LoggerChannelFactoryInterface $loggerFactory) {
    $this->state = $state;
    $this->loggerFactory = $loggerFactory->get('Google Inspection API');
  }

  /*
   * Setup Google Client on instantiation.
   */
  protected function initializeClient() {
    // Get OAuth JSON file
    $file = $this->state->get('google_inspection_api_json_file');
    if (isset($file[0])) {

      // Initialize Google Client
      $this->client = new \Google\Client();
      $uri = File::load($file[0])->getFileUri();

      // Set the OAuth JSON file
      try {
        $this->client->setAuthConfig($uri);
      } catch (Exception $e) {
        $this->loggerFactory->error($e->getMessage());
      }

      // Set the scopes
      $this->client->addScope(\Google\Service\Webmasters::WEBMASTERS);
      $this->client->addScope(\Google\Service\Webmasters::WEBMASTERS_READONLY);

      // Set the redirect URI
      $this->client->setRedirectUri('https://' . $_SERVER['HTTP_HOST'] . '/admin/config/services/google-inspection-api/dashboard');

      // Check if we have an access token and if not, get one.
      if (!$this->client->getAccessToken() && !isset($_GET['code'])) {
        $this->client->setIncludeGrantedScopes(TRUE);
        $authUrl = $this->client->createAuthUrl();
        header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));
        exit;
      }

      // Check if we have an authorization code and if so, exchange it for an
      // access token.
      if (isset($_GET['code'])) {
        // Exchange authorization code for access token.
        $token = $this->client->fetchAccessTokenWithAuthCode($_GET['code']);

        // Check if the access token is expired and refresh it.
        if ($this->client->isAccessTokenExpired()) {
          if ($this->client->getRefreshToken()) {
            $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
          }
          else {
            $this->loggerFactory->error('No refresh token found.');
          }
        }
      }
      else {
        $this->loggerFactory->error('Unable to get token, please visit the <a href="%url">Setting Page</a> and upload a json file', [
            '%url' => '/admin/config/services/google-inspection-api/settings',
          ]
        );
      }
    }
    else {
      $this->loggerFactory->error('Unable to get json file, please visit the <a href="%url">Setting Page</a> and upload a json file', [
          '%url' => '/admin/config/services/google-inspection-api/settings',
        ]
      );
    }
  }


  /**
   * Call the Google Inspection API.
   *
   * @return \Google\Service\SearchConsole\InspectUrlIndexResponse
   */
  protected function callApi(): \Google\Service\SearchConsole\InspectUrlIndexResponse {
    $service = new \Google\Service\SearchConsole($this->client);
    $query = new \Google\Service\SearchConsole\InspectUrlIndexRequest();

    // Set the parameters
    $query->setInspectionUrl($this->state->get('google_inspection_api_inspecturl'));
    $query->setSiteUrl($this->state->get('google_inspection_api_siteurl'));
    $query->setLanguageCode($this->state->get('google_inspection_api_languagecode'));

    // Call the API
    return $service->urlInspection_index->inspect($query);
  }

  /**
   * Inspect the URL.
   *
   * @return \Google\Service\SearchConsole\InspectUrlIndexResponse
   */
  public function inspect() {
    $this->initializeClient();
    return $this->callApi();
  }

}
